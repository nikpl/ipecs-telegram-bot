﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CallBot.PBX;

namespace CallBot.Tests.PBX
{
    [TestFixture]
    [TestOf(typeof(ParsedCall))]
    public class ParsedCallTest
    {
        [Test]
        public void IncomingCall()
        {
            var parser = new ParsedCall("4382 CO12  009 00:00:00 25/02/17 20:03 I79990123756");
            Assert.True(parser.IsIncomingCall);
            Assert.AreEqual(parser.Phone, "79990123756");
        }

        [Test]
        public void IncomingCallShortNumber()
        {
            var parser = new ParsedCall("4382 CO12  009 00:00:00 25/02/17 20:03 I555333");
            Assert.True(parser.IsIncomingCall);
            Assert.AreEqual(parser.Phone, "555333");
        }

        [Test]
        public void IncomingCallMissed()
        {
            var parser = new ParsedCall("4382 CO12  009 00:00:00 25/02/17 20:03 I555333");
            Assert.True(parser.IsMissed);
        }

        [Test]
        public void IncomingCallNotMissed()
        {
            var parser = new ParsedCall("4382 CO12  009 00:00:15 25/02/17 20:03 I555333");
            Assert.False(parser.IsMissed);
        }

        [Test]
        public void NotIncomingCall()
        {
            var parser = new ParsedCall("4382 CO12  009 00:00:00 25/02/17 20:03 O79697203675");
            Assert.False(parser.IsIncomingCall);
        }
    }
}
