﻿using CallBot.PBX;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallBot.Tests.PBX
{
    class FakePbxLogReader : IPbxLogReader
    {
        private string[] linesToReturn;
        private int currentPosition = 0;

        public FakePbxLogReader(string[] linesToReturn)
        {
            this.linesToReturn = linesToReturn;
        }

        public void Dispose()
        { }

        public string ReadLine()
        {
            if (currentPosition < linesToReturn.Length)
                return linesToReturn[currentPosition++];
            else
                throw new PbxConnectionException();
        }
    }

    class FakePbxLogReaderFactory : IPbxLogReaderFactory
    {
        private string[] linesToReturn;

        public FakePbxLogReaderFactory(string[] linesToReturn)
        {
            this.linesToReturn = linesToReturn;
        }

        public IPbxLogReader Create()
        {
            return new FakePbxLogReader(linesToReturn);
        }
    }
}
