﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CallBot.Tests.PBX;
using CallBot.PBX;

namespace CallBot.Tests
{
    [TestFixture]
    [TestOf(typeof(CallObserver))]
    public class CallObserverTest
    {
        [Test]
        public void CunstructorWithoutException()
        {
            var observer = CreateObserverOf(new string[] { });
            Assert.Throws(typeof(PbxConnectionException), () => observer.Watch());
        }

        [Test]
        public void EventEmits()
        {
            var fakeLog = new string[] {
                "4380 110   012 00:01:51 31/06/17 18:17 I555333",
                "4384 120   012 00:00:00 31/06/17 20:03 O79990321010",
                "4382 CO12  009 00:00:00 31/06/17 20:03 I79990123455"
            };
            var observer = CreateObserverOf(fakeLog);
            var outputPhones = new List<String>();
            observer.IncomingCall += (call) => { outputPhones.Add(call.Phone); };
            try
            {
                observer.Watch();
            } 
            catch (PbxConnectionException)
            {
                Assert.AreEqual(outputPhones.ToArray(), new string[] { "555333", "79990123455" });
            }
        }

        private CallObserver CreateObserverOf(string[] logMessgases)
        {
            var fakeReader = new FakePbxLogReader(logMessgases);
            var observer = new CallObserver(fakeReader);
            return observer;
        }
    }
}
