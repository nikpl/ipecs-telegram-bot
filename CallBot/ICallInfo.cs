﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallBot
{
    public interface ICallInfo
    {
        string Phone { get; }
        bool IsMissed { get; }
    }
}
