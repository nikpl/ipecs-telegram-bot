﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallBot
{
    static class Configuration
    {
        private const string PBX_ADDRESS = "PbxAddress";
        private const string PBX_PORT = "PbxPort";
        private const string TELEGRAM_BOT_TOKEN = "TelegramBotToken";
        private const string TELEGRAM_CHAT_ID= "TelegramChatId";
        private const string USE_PROXY = "UseProxy";
        private const string PROXY_ADDRESS = "ProxyAddress";
        private const string PROXY_PORT = "ProxyPort";
        private const string PROXY_USERNAME = "ProxyUsername";
        private const string PROXY_PASSWORD = "ProxyPassword";

        public static string PbxAddress => GetSetting(PBX_ADDRESS);
        public static int PbxPort => GetIntSetting(PBX_PORT);
        public static string TelegramBotToken => GetSetting(TELEGRAM_BOT_TOKEN);
        public static int TelegramChatId => GetIntSetting(TELEGRAM_CHAT_ID);
        public static bool UseProxy => GetBoolSetting(USE_PROXY);
        public static string ProxyAddress => GetSetting(PROXY_ADDRESS);
        public static int ProxyPort => GetIntSetting(PROXY_PORT);
        public static string ProxyUsername => GetSetting(PROXY_USERNAME);
        public static string ProxyPassword => GetSetting(PROXY_PASSWORD);

        private static NameValueCollection AppSettings = System.Configuration.ConfigurationManager.AppSettings;

        private static string GetSetting(string key)
        {
            return AppSettings.Get(key);
        }

        private static int GetIntSetting(string key)
        {
            var settingStr = GetSetting(key);
            if (!Int32.TryParse(settingStr, out int setting))
                throw new Exception();
            return setting;
        }

        private static bool GetBoolSetting(string key)
        {
            var settingStr = GetSetting(key);
            if (!Boolean.TryParse(settingStr, out bool setting))
                throw new System.Configuration.ConfigurationErrorsException();
            return setting;
        }
    }
}
