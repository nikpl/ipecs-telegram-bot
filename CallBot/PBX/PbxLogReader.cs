﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;

namespace CallBot.PBX
{
    public class PbxLogReader: IPbxLogReader, IDisposable
    {
        private TcpClient tcpClient;
        private string ip;
        private int port;
        private StreamReader streamReader;

        public PbxLogReader(string ip, int port)
        {
            this.ip = ip;
            this.port = port;
            tcpClient = new TcpClient();
            Connect();
        }

        private void Connect()
        {
            try
            {
                tcpClient.Connect(ip, port);
                streamReader = new StreamReader(tcpClient.GetStream());
            }
            catch (SocketException)
            {
                throw new PbxConnectionException();
            }
        }

        public void Dispose()
        {
            if (tcpClient != null)
                tcpClient.Dispose();
        }

        public string ReadLine()
        {
            try
            {
                var line = streamReader.ReadLine();
                Console.WriteLine($"Input: {line}");
                return line;
            }
            catch (IOException)
            {
                throw new PbxConnectionException();
            }
            catch (SocketException)
            {
                throw new PbxConnectionException();
            }
        }
    }
}
