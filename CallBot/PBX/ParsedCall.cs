﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CallBot.PBX
{
    public class ParsedCall: ICallInfo
    {
        private const string PHONE_REGEX = "[I,G][0-9]{6,11}"; // I79990123456, G79990123456, I555333 ...
        private string phone;
        private bool isInocmingCall = false;
        private bool isCallMissed = false;


        public ParsedCall(string logLine)
        {
            GetPhoneFromLine(logLine);
            CheckIfCallMissed(logLine);
        }

        private void GetPhoneFromLine(string logLine)
        {
            var matches = Regex.Matches(logLine, PHONE_REGEX);
            if (matches.Count == 1)
            {
                var phoneWithPrefix = matches[0].Value;
                phone = RemoveCallTypePrefix(phoneWithPrefix);
                isInocmingCall = true;
            }
        }
        
        private string RemoveCallTypePrefix(string phone)
        {
            return phone.Substring(1);
        }

        private void CheckIfCallMissed(string logLine)
        {
            isCallMissed = logLine.Contains("00:00:00");
        }

        public bool IsIncomingCall { get => isInocmingCall; }

        public string Phone { get => phone; }

        public bool IsMissed { get => isCallMissed; }
    }
}
