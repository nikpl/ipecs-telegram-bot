﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallBot.PBX
{
    public interface IPbxLogReaderFactory
    {
        IPbxLogReader Create();
    }
}
