﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallBot.PBX
{
    public class PbxLogReaderFromSettingsFactory : IPbxLogReaderFactory
    {
        public IPbxLogReader Create()
        {
            return new PbxLogReader(Configuration.PbxAddress, Configuration.PbxPort);
        }
    }
}
