﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CallBot.PBX;

namespace CallBot
{
    public class CallObserver
    {
        private IPbxLogReader pbxLogReader;

        public CallObserver(IPbxLogReader pbxLogReader)
        {
            this.pbxLogReader = pbxLogReader;
        }

        public delegate void CallEventHandler(ICallInfo call);
        public event CallEventHandler IncomingCall;

        public void Watch()
        {
            while (true)
            {
                var logLine = ReadNextLogLine();
                NotifyIfLineContainsIncomingCall(logLine);
            }
        }

        private string ReadNextLogLine()
        {
            return pbxLogReader.ReadLine();
        }

        private void NotifyIfLineContainsIncomingCall(string logLine)
        {
            var call = new ParsedCall(logLine);
            if (call.IsIncomingCall)
                NotifyAboutIncomingCall(call);
        }

        private void NotifyAboutIncomingCall(ParsedCall call)
        {
            IncomingCall?.Invoke(call);
        }
    }
}
