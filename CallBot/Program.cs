﻿using System;
using CallBot.PBX;

namespace CallBot
{
    class Program
    {
        private static IPbxLogReaderFactory pbxLogReaderFactory;
        private static IMessengerFactory messengerFactory;
        private static IMessenger messenger;

        static void Main(string[] args)
        {
            try
            {
                Initialize();
                MainLoop();
            }
            catch (System.Configuration.ConfigurationErrorsException ex)
            {
                Console.WriteLine("CONFIGURATION ERROR");
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("UNEXPECTED ERROR");
                Console.WriteLine(ex);
            }

            Console.Read();
        }

        private static void Initialize()
        {
            pbxLogReaderFactory = new PbxLogReaderFromSettingsFactory();
            messengerFactory = new TelegramFromSettingsFactory();
            messenger = messengerFactory.Create();
        }

        private static void MainLoop()
        {
            while (true)
            {
                Console.WriteLine("Connecting to PBX ...");
                ConnectToPbxAndStartSendMessages();
                Console.WriteLine("Connected");
            }
        }

        private static void ConnectToPbxAndStartSendMessages()
        {
            try
            {
                using (var pbxLogReader = pbxLogReaderFactory.Create())
                {
                    var callObserver = new CallObserver(pbxLogReader);
                    callObserver.IncomingCall += CallObserver_IncomingCall;
                    callObserver.Watch();
                }
            }
            catch (PbxConnectionException)
            {
                Console.WriteLine("PBX CONNECTION PROBLEM");
            }
        }

        private static void CallObserver_IncomingCall(ICallInfo call)
        {
            Console.WriteLine($"Incoming call from {call.Phone}");
            var message = BuildMessage(call);
            SendMessage(message);
        }

        private static string BuildMessage(ICallInfo call)
        {
            var message = $"Звонок от {call.Phone}";
            if (call.IsMissed)
                message += " - пропущен";
            return message;
        }

        private static async void SendMessage(string message)
        {
            Console.WriteLine($"Sending message: {message}");
            await messenger.SendMessage(message);
        }

    }
}
