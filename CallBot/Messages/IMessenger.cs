﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CallBot
{
    public interface IMessenger
    {
        Task SendMessage(string message);
    }
}
