﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;

namespace CallBot
{
    class Telegram : IMessenger
    {
        private TelegramBotClient botClient;
        private int chatId;

        public Telegram(string token, int chatId)
        {
            this.chatId = chatId;
            botClient = new TelegramBotClient(token);
        }

        public Telegram(string token, int chatId, IWebProxy proxy)
        {
            this.chatId = chatId;
            botClient = new TelegramBotClient(token, proxy);
        }

        public async Task SendMessage(string message)
        {
            try
            {
                await botClient.SendTextMessageAsync(chatId, message);
            }
            catch (Exception)
            {
                Console.WriteLine($"ERROR ON SENDING MESSAGE: {message}");
            }
        }


    }
}
