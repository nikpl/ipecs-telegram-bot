﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CallBot
{
    class TelegramFromSettingsFactory : IMessengerFactory
    {
        public IMessenger Create()
        {
            var token = Configuration.TelegramBotToken;
            var chatId = Configuration.TelegramChatId;
            if (!Configuration.UseProxy)
            {
                return new Telegram(token, chatId);
            }
            else
            {
                var proxy = CreateProxyFromSettings();
                return new Telegram(token, chatId, proxy);
            }
        }

        private IWebProxy CreateProxyFromSettings()
        {
            return new WebProxy(Configuration.ProxyAddress, Configuration.ProxyPort)
            {
                Credentials = new NetworkCredential(Configuration.ProxyUsername, Configuration.ProxyPassword)
            };
        }
    }
}
